const path = require('path');
const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');
const Sequelize = require('sequelize');
const service = require('feathers-sequelize');

const serviceLoader = require('./ServiceLoader');

const sequelize = new Sequelize('featherstutorial', 'featherstutorial', 'Rup@123', {
    host: 'localhost',
    dialect: 'mysql',

    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },

    operatorsAliases: false
});



// Create an Express compatible Feathers application instance.
const app = express(feathers());

// Turn on JSON parser for REST services
app.use(express.json());
// Turn on URL-encoded parser for REST services
app.use(express.urlencoded({ extended: true }));
// Enable REST services
app.configure(express.rest());
// Enable Socket.io services
app.configure(socketio());

app.use(express.errorHandler());


serviceLoader.loadServices(sequelize, app);


// Start the server
const port = 3030;

app.listen(port, () => {
    console.log(`Feathers server listening on port ${port}`);
});
