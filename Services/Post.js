
const Sequelize = require('sequelize');

var exports = module.exports = {};

exports.createAndUsePostService = function(sequelize, app ) {

    const Post = sequelize.define('posts', {
            content: Sequelize.STRING,
            status: Sequelize.STRING
        },
        {
            timestamps: false,
            underscored: true,
        });

    const service4Post = {
        find(params) {
            let posts = Post.findAll({});
            return Promise.resolve(posts);
        },
        get(id, params) {
        },
        create(data, params) {
        },
        update(id, data, params) {
        },
        patch(id, data, params) {
        },
        remove(id, params) {
        },
        setup(app, path) {
        }
    };

    app.use('/posts', service4Post);
};
